# -*- coding: utf-8 -*-

import pprint
import random

import numpy

# 19 novembre 2023
cochon1 = {
    'nom': "Cochon1",
    "parts": {
        "Matthieu": 0.25,
        "Thomas": 0.125,
        "Jean": 0.125,
        "Adrien": 0.25,
        "Maxime": 0.25,
    },
    ## Rouge

    ## vret
    "saucissons-comte": {
        1:353,
        2:325,
        3:369,
        4:370,
        5:420,
        6:270,
        7:392,
        8:380,
        9:369,
        10:408,
        11:331,
        12:345,
        13:343,
        14:341,
        15:382,
        16:456,
        17:436,
        18:421,
    },

    ## rouges
    "saucissons-nature": {
        19:330,
        20:354,
        21:339,
        22:399,
        23:352,
        24:330,
        25:306,
        26:382,
        27:324,
        28:355,
        29:330,
        30:355,
        31:362,
        32:367,
        33:352,
        34:329,
        35:322,
        36:385,
        37:350,
        38:451,
        39:311,
        40:413,
        41:401,
        42:396,
        43:348,
        44:466,
        45:358,
        46:357,
        47:395,
        48:351,
        49:438,
        50:367,
        
    }
    
    ## Rouge
    #'saucisses-chorizo': {
    #    1: 316,
    #    2: 204,
    #    3: 296,
    #    4: 366,
    #    5: 377,
    #    6: 307,
    #    7: 330,
    #    8: 339,
    #    9:  352,
    #},
    ## Vert
    #'poireaux': {
    #    10: 350,
    #    11: 295,
    #    12: 317,
    #    13: 347,
    #    14: 343,
    #    15: 346,
    #    16: 277,
    #    17: 354,
    #    18: 368,
    #    19: 358,
    #    20: 320,
    #    21: 270,
    #    22: 340,
    #    23: 421,
    #    24: 311,
    #},
    ## Gris
    #'saucisses-armeniennes': {
    #    25: 309,
    #    26: 362,
    #    27: 298,
    #    28: 326,
    #    29: 304,
    #    30: 323,
    #    31: 297, 
    #    32: 360, 
    #    33: 452, 
    #    34: 440, 
    #    35: 325, 
    #},
    # # Rose
    # # 'saucissons-pistache': {
    # # },
    # # Bleu
    # 'saucissons-noix': {
    #     78: 564,
    #     77: 539,
    #     73: 400,
    #     76: 499,
    #     75: 519,
    #     74: 465,
    #     94: 480,
    #     71: 541,
    #     70: 578,
    #     72: 603,
    #     69: 558,
    #     80: 234,
        
    # },
    # # Rose
    # 'saucissons-nature': {
    #     86: 560,
    #     88: 552,
    #     82: 580,
    #     104: 410,
    #     102: 515,
    #     98: 554,
    #     93: 569,
    #     103: 435,
    #     79: 477,
    #     100: 503,
    #     85: 454,
    #     81: 481,
    #     83: 472,
    #     101: 351,
    #     95: 558,
    #     90: 583,
    #     84: 540,
    #     87: 501,
    #     99: 555,
    #     92: 561,
    #     89: 304,
    #     96: 550,
    #     91: 515,
    #     97: 542,
    # },
}

# 26 novembre 2022
cochon2 = {
    'nom': "Cochon2",
    "parts": {
        "Charlie": 0.25,
        "Olivier": 0.25,
        "Jean-Christophe": 0.25,
        "Stéphanie": 0.25,
    },

    "orange-poivre": {
        
    },
    
    "bleu-nature": {
        1: 305,
        2: 369,
        3: 340,
        4: 377,
        5: 343,
        6: 280,
        7: 366,
        8: 396,
        9: 432,
        10: 357,
        11: 333,
        12: 493,
        13: 387,
        14: 266,
        15: 303,
        16: 340,
        17: 342,
        18: 314,
        19: 349,
        20: 320,
        21: 377,
        22: 357,
        23: 349,
        24: 441,
        25: 310,
        26: 327,
        27: 400,
        28: 355,
        29: 324,
        30: 312,
        31: 400,
        32: 321,
        33: 379,
        34: 278,
        35: 310,
        36: 392,
        37: 238,
        38: 258,
        39: 400,
        40: 427,
        41: 288,
    },
    "Rose-noix-comté": {
        42: 320,
        43: 346,
        44: 330,
        45: 339,
        46: 236,
        47: 321,
        48: 276,
        49: 263,
        50: 394,
        51: 231,
        52: 479,
        53: 188,
        54: 393,
        55: 229,
        56: 416,
        57: 306,
        #58: ,
        #59: ,
        #60: ,
        #1: ,
        #2: ,
        #3: ,
        #4: ,
        #5: ,
        #6: ,
        #7: ,
        #8: ,
        #9: ,
        #10: ,
    },

    "Orange-poivre" : {
        62: 163,
        63: 430,
        64: 348,
        65: 327,
        66: 374,
    },

    "Vert-pistache": {
        71: 281,
        58: 354,
        59: 394,
        61: 279,
        60: 309,
        73: 239,
        67: 368,
        68: 351,
        69: 377,
        70: 263,
        72: 311,
    },

    
    
    #"saucisses-nature": {
    #    1: 376,
    #    2: 361,
    #    3: 407,
    #    4: 379,
    #    5: 319,
    #    6: 350,
    #    7: 288,
    #    8: 330,
    #    9: 373,
    #    10: 112,
    #    11: 407,
    #    12: 300,
    #    13: 393,
    #    14: 328,
    #    15: 416,
    #    16: 335,
    #    17: 340,
    #    18: 425,
    #},
    
    # 'chorizo': {
    #     1: 213,
    #     25: 231,
    #     2: 341,
    #     3: 453,
    #     4: 334,
    #     5: 338,
    #     6: 303,
    # },
    # 'saucisses-poireaux': {
    #     9: 255,
    #     28: 236, 
    #     17: 354,
    #     18: 273,
    #     19: 279,
    #     20: 258,
    #     21: 300,
    #     22: 310,
    #     23: 300,
    #     26: 132,
    #     27: 149,
    #     24: 179,
    # },
    # 'saucisses-armeniennes': {
    #     7: 348,
    #     8: 456,
    #     10: 308,
    #     11: 376,
    #     12: 371,
    #     13: 355,
    #     14: 441,
    #     15: 422,
    #     16: 270,
    # },
    # vert clair
    # 'saucissons-pistache': {
    #     19:583,
    #     21:578,
    #     22:681,
    #     20:485,
    #     17:567,
    #     18:450,
    #     16:521,
    #     15:584,
    #     14:544,
    #     13:506,
    #     
    # },
    # # blanc
    # 'saucissons-nature': {
    #     40:512,
    #     39:510,
    #     23:615,
    #     24:466,
    #     25:500,
    #     26:531,
    #     27:580,
    #     28:494,
    #     29:631,
    #     30:540,
    #     31:545,
    #     32:526,
    #     33:515,
    #     34:591,
    # },
    # # violet
    # 'saucissons-comte-noisette': {
    #     10:251,
    #     11:450,
    #     8:615,
    #     7:563,
    #     9:561,
    #     12:512,
    #     6:587,
    #     5:558,
    #     4:653,
    #     3:613,
    #     2:616,
    #     1:448,
    # },
}

# 18 janvier
cochon3 = {
    'nom': "Hubert",
    "parts": {
        "Jean-Lou": 0.5,
        "Alex": 0.25,
        "Thomas": 0.125,
        "Melodie": 0.125,
    },
    "comté-noisettes-bleu": {
        74: 343,
        75: 349,
        76: 356,
        77: 357,
        78: 309,
        79: 306,
        80: 246,
        81: 226,
        82: 243,
        83: 214,
        84: 343,
        85: 167,
        86: 326,
        87: 306,
        88: 294,
        89: 317,
        90: 320,
        91: 342,
        
    },
    "nature-jaune": {
        51:304,
        52:399,
        53:370,
        54:316,
        55:274,
        56:356,
        57:276,
        58:317,
        59:296,
        60:300,
        61:346,
        62:309,
        63:310,
        64:279,
        65:300,
        66:317,
        67:483,
        68:310,
        69:351,
        70:340,
        71:321,
        72:330,
        73:284,
        74:284,
        75:313,
        76:355,
        77:250,
        78:334,
        79:285,
        80:280,
        81:474,
        82:291,
        83:338,
        84:448,
        85:267,
        86:200,
        87:186,
    },
    ## vert poireaux
    #"saucisse-poireaux": {
    #    14: 233,
    #    15: 216,
    #    16: 216,
    #    17: 180,
    #    18: 231,
    #    19: 256,
    #    20: 228,
    #    22: 235,
    #},
    #
    ## rose chorizo
    #"saucisse-chorizo": {
    #    1: 280,
    #    2: 277,
    #    3: 289,
    #    4: 225,
    #    5: 256,
    #    21: 284,
    #},
    ## violet : armeinnne
    #"saucisse-armenienne": {
    #    8: 245,
    #    9: 240,
    #    10: 247,
    #    11: 260,
    #    12: 315,
    #    13: 184,
    #},
    # blanc nature
    

}


class DispatchPig:
    def __init__(self, pig):
        self.pig = pig
        self.base_part = min([self.parts[x] for x in self.parts])
        self.total_weight = 0
        self.total_weight_by_nature = {}
        self.base_part_weight_by_nature = {}
        for nature in self.natures:
            self.total_weight_by_nature[nature] = sum([self.pig[nature][x] for x in self.pig[nature]])
            self.total_weight += self.total_weight_by_nature[nature]
            self.base_part_weight_by_nature[nature] = self.total_weight_by_nature[nature] * self.base_part

    def display_base_info(self):
        print(f"\n\n######## {self.pig['nom']} ########\n\n")
        print(f"Part de base {self.base_part}")
        print(f"Poids total {self.total_weight}")
        for nature in self.natures:
            print(f"\n**** {nature} ****\n")
            print(f"Poids d'une part {self.base_part_weight_by_nature[nature]}")
            print(f"Liste des saucissons {self.items(nature)}")

    @property
    def parts(self):
        return self.pig['parts']
    
    @property
    def persons(self):
        return list(self.pig['parts'].keys())
    
    @property
    def natures(self):
        natures = list(self.pig.keys())
        natures.remove('parts')
        natures.remove('nom')
        return natures

    def items(self, nature):
        return list(self.pig[nature].keys())

    def display_dispatch(self):
        dispatch = self.dispatch_all()

        print(f"\n\n######## {self.pig['nom']} ########")
        for nature in self.natures:
            print(f"\n\n--- {nature} --------------")
            pprint.pprint(dispatch[nature])

        return dispatch

    def dispatch_all(self):
        dispatch = {}
        for nature in self.natures:
            print(f"\nTraitement des {nature}")
            print("[CTRL]+[c] quand l'écart type est satisfaisant !")
            dispatch[nature] = self.dispatch(nature)

        return dispatch
            
    def dispatch(self, nature):  # NOQA:C901
        best = None
        while True:
            try:
                randomized_items = self.items(nature)
                random.shuffle(randomized_items)
                r = {}
                for person in self.persons:
                    r[person] = {
                        'saucissons': [],
                        'poids': 0,
                        'ecart': 0,
                        'cible': self.base_part_weight_by_nature[nature] * self.parts[person] / self.base_part,
                    }
                
                persons = self.persons
                person = persons.pop(0)
                obj = self.base_part_weight_by_nature[nature] * self.parts[person] / self.base_part
                for saucisson in randomized_items:
                    r[person]['saucissons'].append(saucisson)
                    r[person]['saucissons'].sort()
                    r[person]['poids'] += self.pig[nature][saucisson]
                
                    if r[person]['poids'] > obj * 0.97:
                        r[person]['ecart'] = r[person]['poids'] - r[person]['cible']
                        try:
                            person = persons.pop(0)
                        except IndexError:
                            break
                        else:
                            obj = self.base_part_weight_by_nature[nature] * self.parts[person] / self.base_part
                else:
                    r[person]['ecart'] = r[person]['poids'] - r[person]['cible']
                
                r['ecart_type'] = numpy.std([r[x]['ecart'] for x in self.persons])
                
                if best is None:
                    best = r
                
                if r['ecart_type'] < best['ecart_type']:
                    best = r
                    print(f" Meilleur écart {best['ecart_type']}")
                
                if best['ecart_type'] < 1:
                    break
            except KeyboardInterrupt:
                print()
                break
                
        return best


# dispatch_cochon1 = DispatchPig(cochon1)
# dispatch_cochon1.display_base_info()
# pprint.pprint(dispatch_cochon1.display_dispatch())

# dispatch_cochon2 = DispatchPig(cochon2)
# dispatch_cochon2.display_base_info()
# dispatch_cochon2.display_dispatch()

dispatch_cochon3 = DispatchPig(cochon3)
dispatch_cochon3.display_base_info()
pprint.pprint(dispatch_cochon3.display_dispatch())
