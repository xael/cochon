######## Cochon2 ########


--- saucisses-poireaux --------------
{'Copain Matthieu': {'cible': 609.125,
                     'ecart': -3.125,
                     'poids': 606,
                     'saucissons': [43, 47]},
 'Jean-Lou': {'cible': 2436.5,
              'ecart': 2.5,
              'poids': 2439,
              'saucissons': [44, 46, 48, 50, 52, 54, 55, 56, 59]},
 'Olivier': {'cible': 1218.25,
             'ecart': -0.25,
             'poids': 1218,
             'saucissons': [40, 41, 45, 49, 53]},
 'Papa Matthieu': {'cible': 609.125,
                   'ecart': 0.875,
                   'poids': 610,
                   'saucissons': [51, 57, 58]},
 'ecart_type': 2.0520568949227505}


--- saucisses-armeniennes --------------
{'Copain Matthieu': {'cible': 411.0,
                     'ecart': -11.0,
                     'poids': 400,
                     'saucissons': [34]},
 'Jean-Lou': {'cible': 1644.0,
              'ecart': 2.0,
              'poids': 1646,
              'saucissons': [33, 35, 37, 39, 42]},
 'Olivier': {'cible': 822.0,
             'ecart': 2.0,
             'poids': 824,
             'saucissons': [36, 38]},
 'Papa Matthieu': {'cible': 411.0,
                   'ecart': 7.0,
                   'poids': 418,
                   'saucissons': [60]},
 'ecart_type': 6.670832032063167}


--- saucisses-chorizo --------------
{'Copain Matthieu': {'cible': 171.625,
                     'ecart': 124.375,
                     'poids': 296,
                     'saucissons': [29]},
 'Jean-Lou': {'cible': 686.5,
              'ecart': 90.5,
              'poids': 777,
              'saucissons': [28, 31, 32]},
 'Olivier': {'cible': 343.25, 'ecart': -343.25, 'poids': 0, 'saucissons': []},
 'Papa Matthieu': {'cible': 171.625,
                   'ecart': 128.375,
                   'poids': 300,
                   'saucissons': [30]},
 'ecart_type': 198.7209687916703}

# A la main chorizo 28 pour Olivier
