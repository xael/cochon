######## Griska ########


--- saucisses-poireaux --------------
{'Alexandre': {'cible': 525.5,
               'ecart': 26.5,
               'poids': 552,
               'saucissons': [3, 7]},
 'Bernard': {'cible': 525.5,
             'ecart': -44.5,
             'poids': 481,
             'saucissons': [5, 6]},
 'Guillaume': {'cible': 525.5,
               'ecart': -13.5,
               'poids': 512,
               'saucissons': [1, 4]},
 'Stéphanie': {'cible': 525.5,
               'ecart': 31.5,
               'poids': 557,
               'saucissons': [2, 8, 9]},
 'ecart_type': 31.052375110448477}


--- saucisses-armeniennes --------------
{'Alexandre': {'cible': 579.75,
               'ecart': -0.75,
               'poids': 579,
               'saucissons': [10, 14]},
 'Bernard': {'cible': 579.75,
             'ecart': -17.75,
             'poids': 562,
             'saucissons': [16, 17]},
 'Guillaume': {'cible': 579.75,
               'ecart': 31.25,
               'poids': 611,
               'saucissons': [12, 15]},
 'Stéphanie': {'cible': 579.75,
               'ecart': -12.75,
               'poids': 567,
               'saucissons': [11, 13]},
 'ecart_type': 19.070592544543548}
